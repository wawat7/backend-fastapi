from flask_inputs import Inputs
from flask_inputs.validators import JsonSchema
import jsonschema

book_validator = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string', 'minLength': 1},
    },
    'required': ['name']
}


class CreateBookValidator(Inputs):
    json = [JsonSchema(schema=book_validator)]
