from flask import Flask
from configs.environtment_config import get_environment_variables
from routes.v1 import register_routes
from configs.database_config import init_database


def create_app():
    env = get_environment_variables()

    database = init_database(env, 'mongo')

    app = Flask(__name__)

    register_routes(app, database)

    @app.get("/")
    def main():
        return {"message": "app running well..."}

    return app
