from .sql_config import init_sql
from .mongo_config import init_mongo
from pymongo.collection import Collection
from pydantic import BaseModel

class DatabaseConfig:
    mongo: Collection
    sql: any
    db_used: str

    def __init__(self, mongo, sql, db_used):
        self.mongo = mongo
        self.sql = sql
        self.db_used = db_used


def init_database(env, type_db='mongo'):
    return DatabaseConfig(
        mongo= init_mongo(env),
        sql= init_sql(env),
        db_used= type_db
    )
