# import os
# from dotenv import load_dotenv
#
# load_dotenv()


def get_environment_variables():

    return {
        "API_VERSION": "1.0.0",
        "APP_NAME": "TEST BACKEND",
        "DATABASE_DIALECT": "postgresql",
        "DATABASE_HOSTNAME": "localhost",
        "DATABASE_NAME": "bookstore",
        "DATABASE_PORT": 5432,
        "DATABASE_USERNAME": "postgres",
        "DATABASE_PASSWORD": "postgres",
        "MONGO_HOST": "localhost",
        "MONGO_DB": "bookstore",
        "MONGO_USERNAME": "mongo",
        "MONGO_PASSWORD": "mongo",
        "MONGO_PORT": 27018,
        "DEBUG_MODE": True,
    }
