from pymongo import MongoClient

def connect_mongo(MONGO_HOST, MONGO_PORT, MONGO_DB, MONGO_USERNAME, MONGO_PASSWORD):
    try:

        mongo_url = f"mongodb://{MONGO_USERNAME}:{MONGO_PASSWORD}@{MONGO_HOST}:{MONGO_PORT}"
        mongo = MongoClient(mongo_url)
        db = mongo[MONGO_DB]
        mongo.server_info()

        print("DATABASE MONGO CONNECTED")
        return db
    except:
        raise Exception("Cannot connect MONGO")


def init_mongo(env):
    return connect_mongo(
        MONGO_HOST=env['MONGO_HOST'],
        MONGO_PORT=env['MONGO_PORT'],
        MONGO_DB=env['MONGO_DB'],
        MONGO_USERNAME=env['MONGO_USERNAME'],
        MONGO_PASSWORD=env['MONGO_PASSWORD'],
    )