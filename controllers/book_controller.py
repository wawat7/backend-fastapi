from repositories.book_repository import BookRepository
from services.book_service import BookService
from transforms.book_transform import transform_books, transform_book
from schemas.book_schema import BookPostRequestSchema, BookSchema
from configs.database_config import DatabaseConfig
from utils.object_id_utils import is_valid_objectid
from responses.bad_request_response import bad_request_response
from adapters.validators.book.create_book_validator import CreateBookValidator
from adapters.validators.book.update_book_validator import UpdateBookValidator
from responses.success_response import success_response
from flask import request


class BookController:
    book_service: BookService
    database: DatabaseConfig

    def __init__(self, database: DatabaseConfig):
        book_repository = BookRepository(database)
        self.book_service = BookService(book_repository)
        self.database = database

    def get_all_books(self):
        return success_response(transform_books(self.book_service.all()), 'list books')

    def get_book_by_id(self, id: str):
        if self.database.db_used == 'mongo' and not is_valid_objectid(id):
                return bad_request_response('id not valid', {})

        book = self.book_service.find_by_id(id)
        return success_response(transform_book(book), 'detail book')

    def create_book(self, request: request):
        validator = CreateBookValidator(request)

        if not validator.validate():
            return bad_request_response('BAD REQUEST', validator.errors)

        payload = request.get_json()
        inserted_id = self.book_service.create(BookPostRequestSchema(**{
            "name": payload['name']
        }))
        return success_response(
            transform_book(self.book_service.find_by_id(str(inserted_id))),
            'create book successfully'
        )


    def update_book(self, id: str, request: request):
        if self.database.db_used == 'mongo' and not is_valid_objectid(id):
                return bad_request_response('id not valid', {})

        validator = UpdateBookValidator(request)
        if not validator.validate():
            return bad_request_response('BAD REQUEST', validator.errors)

        payload = request.get_json()
        self.book_service.update_by_id(id, BookPostRequestSchema(**{
            "name": payload['name']
        }))
        return success_response(transform_book(self.book_service.find_by_id(id)), 'update book successfully')

    def delete_book(self, id: str):
        if self.database.db_used == 'mongo' and not is_valid_objectid(id):
                return bad_request_response('id not valid', {})

        self.book_service.delete_by_id(id)
        return success_response({}, 'delete book successfully')
