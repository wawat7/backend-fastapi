from pymongo import collection
from bson.objectid import ObjectId
from models.book_model import Book
from schemas.book_schema import BookSchema

class BookQuery:
    db: collection.Collection
    __collection__ = "books"

    def __init__(self, db):
        try:
            self.db = db[self.__collection__]
        except Exception as e:
            print(e)

    def all(self):
        return [Book(**{"id": str(book['_id']), "name": book['name']}) for book in self.db.find()]

    def find_by_id(self, id: str):
        book = self.db.find_one({"_id": ObjectId(id)})
        return Book(**{"id": str(book['_id']), "name": book['name']})

    def create(self, book: BookSchema) -> str:
        return self.db.insert_one(dict(book)).inserted_id

    def update(self, id: str, book: BookSchema):
        return self.db.update_one({"_id": ObjectId(id)}, {"$set": dict(book)})

    def delete(self, id: str):
        return self.db.delete_one({"_id": ObjectId(id)})

