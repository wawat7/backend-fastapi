from pymongo import collection
from models.user_model import User

class UserQuery:
    db: collection.Collection
    __collection__ = "users"

    def __init__(self, db):
        try:
            self.db = db[self.__collection__]
        except Exception as e:
            print(e)


    def create_user(self, user: User):
        return self.db.insert_one(dict(user)).inserted_id

    def get_user_by_username(self, username: str):
        return self.db.find_one({"username": username})