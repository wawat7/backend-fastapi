from pymongo import collection
from bson.objectid import ObjectId
from models.book_model import Book

class BookQuery:
    db: collection.Collection
    __collection__ = "books"

    def __init__(self, db):
        try:
            self.db = db[self.__collection__]
        except Exception as e:
            print(e)

    def all(self):
        # Not Implemented
        return "get all books"

    def find_by_id(self, id: str):
        # Not Implemented
        return "find_by_id"

    def create(self, book: Book) -> str:
        # Not Implemented
        return "create"

    def update(self, id: str, book: Book):
        # Not Implemented
        return "update"

    def delete(self, id: str):
        # Not Implemented
        return "delete"

