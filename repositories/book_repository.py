from queries.mongo.book_query import BookQuery as MongoBookQuery
from queries.sql.book_query import BookQuery as SqlBookQuery
from schemas.book_schema import BookSchema
from configs.database_config import DatabaseConfig
class BookRepository:
    database: DatabaseConfig
    mongo_book_query: MongoBookQuery
    sql_book_query: SqlBookQuery

    def __init__(self, db: DatabaseConfig):
        self.database = db
        self.mongo_book_query = MongoBookQuery(db.mongo)
        self.sql_book_query = SqlBookQuery(db.sql)

    def all(self):
        if self.database.db_used == 'mongo':
            return self.mongo_book_query.all()
        else:
            return self.sql_book_query.all()

    def find_by_id(self, id: str):
        if self.database.db_used == 'mongo':
            return self.mongo_book_query.find_by_id(id)
        else:
            return self.sql_book_query.find_by_id(id)

    def create(self, book: BookSchema) -> str:
        if self.database.db_used == 'mongo':
            return self.mongo_book_query.create(book)
        else:
            return self.sql_book_query.create(book)

    def update(self, id: str, book: BookSchema):
        if self.database.db_used == 'mongo':
            return self.mongo_book_query.update(id, book)
        else:
            return self.sql_book_query.update(id, book)

    def delete(self, id: str):
        if self.database.db_used == 'mongo':
            return self.mongo_book_query.delete(id)
        else:
            return self.sql_book_query.delete(id)
