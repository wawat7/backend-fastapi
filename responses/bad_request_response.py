def bad_request_response(message, errors):
    return {
        "message": message,
        "errors": errors,
    }, 400
