def success_response(
        data,
        message,
):
    return {
        "message": message,
        "data": data,
    }, 200
