from flask import Flask, request, Blueprint
from controllers.book_controller import BookController
from responses.success_response import success_response
from responses.bad_request_response import bad_request_response
from schemas.book_schema import BookPostRequestSchema
from adapters.validators.book.create_book_validator import CreateBookValidator
from utils.object_id_utils import is_valid_objectid
from configs.database_config import DatabaseConfig

def book_route(app: Flask, db: DatabaseConfig, prefix):
    controller = BookController(db)
    book_blueprint = Blueprint('books', __name__)

    @book_blueprint.route('/', methods=['GET'])
    def get_all_books():
        return controller.get_all_books()

    @book_blueprint.route('/<id>', methods=['GET'])
    def get_book_by_id(id: str):
        return controller.get_book_by_id(id)

    @book_blueprint.route('/', methods=['POST'])
    def create_book():
        return controller.create_book(request)

    @book_blueprint.route('/<id>', methods=['PUT'])
    def update_book(id: str):
        return controller.update_book(id, request)
    #
    @book_blueprint.route('/<id>', methods=['DELETE'])
    def delete_book(id: str):
        return controller.delete_book(id)

    app.register_blueprint(book_blueprint, url_prefix=f"{prefix}/books")
