from pydantic import BaseModel

class BookPostRequestSchema(BaseModel):
    name: str

class BookSchema(BookPostRequestSchema):
    id: str
