from queries.mongo.user_query import UserRepository
from fastapi import Depends

class AuthenticationService:
    userRepository: UserRepository

    def __init__(self, user_repository: UserRepository = Depends()):
        self.userRepository = user_repository

    def login(self, username: str, password: str):
        user = self.userRepository.get_user_by_username(username)