from repositories.book_repository import BookRepository
from fastapi import Depends
from schemas.book_schema import BookSchema


class BookService:
    bookRepository: BookRepository

    def __init__(self, book_repository: BookRepository = Depends()):
        self.bookRepository = book_repository

    def create(self, book: BookSchema):
        try:
            inserted_id = self.bookRepository.create(book)
            return inserted_id
        except Exception as exception:
            raise Exception(str(exception))

    def all(self):
        try:
            return self.bookRepository.all()
        except Exception as exception:
            raise Exception(str(exception))

    def find_by_id(self, id: str):
        try:
            return self.bookRepository.find_by_id(id)
        except Exception as exception:
            raise Exception(str(exception))

    def update_by_id(self, id: str, book: BookSchema):
        try:
            checkBook = self.find_by_id(id)
            if not checkBook:
                raise Exception("book not found")

            self.bookRepository.update(id, book)
            return

        except Exception as exception:
            raise Exception(str(exception))

    def delete_by_id(self, id: str):
        try:
            book = self.find_by_id(id)
            if not book:
                raise Exception("book not found")

            self.bookRepository.delete(id)
            return
        except Exception as exception:
            raise Exception(str(exception))

