from models.book_model import Book


def transform_book(book: Book):
    return {
        "id": book.id,
        "name": book.name
    }


def transform_books(books: [Book]):
    formats = []
    for book in books:
        formats.append(transform_book(book))
    return formats
