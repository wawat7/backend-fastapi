from bson import ObjectId


def is_valid_objectid(s):
    try:
        ObjectId(s)
        return True
    except:
        return False
